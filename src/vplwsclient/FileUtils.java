/*
 * Copyright 2018-2023: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee, Titouan Mazier
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package vplwsclient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Helper class with file system management and file contents analysis.
 * In all this class, we will try to follow the following convention :
 * - projectPath/projectFolder will refer to the location from where .vplignore file descibe files.
 * - file/folder/subFolder will refer to folder/files that we are currently dealing with in given function.
 * - vplignoreFolder will refer to the folder containing the .vplignore file.
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 * @author Titouan Mazier
 */
public final class FileUtils {

	private FileUtils() {
	}

	/**
	 * If a vplignore file exists in the given directory, this methods will
	 * parse it and returns its lines.
	 *
	 * @param vplignoreFolder The path to the folder containing .vplignore file.
	 */
	public static List<String> scanExcludedList(String vplignoreFolder) {
		File vplignore = new File(vplignoreFolder + File.separator + ".vplignore");
		try (Scanner reader = new Scanner(vplignore)) {
			List<String> excludedFiles = new ArrayList<>();
			while (reader.hasNextLine()) {
				String line = reader.nextLine().trim();
				if (!line.startsWith("#") && !line.equals("")) {
					excludedFiles.add(line);
				}
			}
			return excludedFiles;
		} catch (FileNotFoundException e) {
			return Collections.emptyList();
		}
	}

	/**
	 * Determines whether or not the specified file has to be excluded from export.
	 *
	 * @param file  The name of the file to consider.
	 * @param excludedFiles A list of glob expressions matching with files that
	 *                      should be excluded. As returned by {@code scanExcludedList}.
	 * @return {@code true} if the file has to be excluded, {@code false} otherwise.
	 */
	public static boolean isExcluded(String file, List<String> excludedFiles) {
		for (String excludedFile : excludedFiles) {
			PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + excludedFile);
			if (matcher.matches(Path.of(file))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Determines whether or not the specified file has to be excluded from export.
	 *
	 * @param file  The name of the file to consider.
	 * @param projectPath   The absolute path to your project (i.e. from which files ar described in .vplignore).
	 * @param excludedFiles A list of glob expressions matching with files that
	 *                      should be excluded. As returned by {@code scanExcludedList}.
	 */
	private static boolean isExcluded(File file, String projectPath, List<String> excludedFiles) {
		projectPath = projectPath.replace(File.separator, "/");
		String projectRelativePath = Paths.get(file.getAbsolutePath())
				.normalize()
				.toString()
				.replace(File.separator, "/")
				.replace(projectPath + "/", "");
		return isExcluded(projectRelativePath, excludedFiles);
	}

	/**
	 * Returns a list of all the files contained within the specified directory and
	 * not excluded.
	 *
	 * @param folder The directory of which the files will be listed.
	 * @param projectPath   The absolute path to your project (i.e. from which files ar described in .vplignore).
	 * @param excludedFiles The list of glob expressions returned by
	 *                      {@code scanExcludedList(projectPath)}
	 * @return A list containing all files in rootDirectory and in sub-directories.
	 * This list does not include these directories, only the files they
	 * contain.
	 */
	public static List<File> listIncludedFiles(String folder, String projectPath, List<String> excludedFiles) {
		List<File> files = new ArrayList<>();
		listIncludedFiles(folder, files, projectPath, excludedFiles);
		return files;
	}

	/**
	 * Recursive function helper for {@link #listFiles(String)}, incrementally
	 * building the list.
	 *
	 * @param folder 	 The path where we are currently searching.
	 * @param collectedFiles     The incrementally built list.
	 * @param projectPath        The absolute path to your project (i.e. from which files ar described in .vplignore).
	 * @param excludedFiles      The list of glob expressions returned by {@code scanExcludedList(projectPath)}.
	 */
	private static void listIncludedFiles(String folder, List<File> collectedFiles, String projectPath,
										  List<String> excludedFiles) {
		File root = Paths.get(folder).toAbsolutePath().normalize().toFile();
		File[] files = root.listFiles();

		if (files == null) {
			return;
		}

		for (File file : files) {
			if (file.isDirectory()) {
				listIncludedFiles(file.getAbsolutePath(), collectedFiles, projectPath, excludedFiles);
			} else {
				if (!isExcluded(file, projectPath, excludedFiles)) {
					collectedFiles.add(file);
				}
			}
		}
	}

	/**
	 * Recursively delete folder with it's files after making sure not to delete any
	 * of vplignored files
	 *
	 * @param file          The folder to delete
	 * @param projectPath   The path to the project root
	 * @param excludedFiles The list of glob expressions returned by
	 *                      {@code scanExcludedList(projectPath)}
	 * @return {@code true} if given folder {@code file} has been deleted,
	 * {@code false} otherwise
	 * @throws IOException
	 */
	public static boolean deleteIncludedFolder(File file, String projectPath, List<String> excludedFiles)
			throws IOException {
		if (!file.exists())
			return false;
		File[] contents = file.listFiles();
		boolean empty = true;
		if (contents != null) {
			// file is a directory - delete its contents first
			for (File f : contents) {
				if (!Files.isSymbolicLink(f.toPath())) {
					// Do not follow symbolic links
					if (!deleteIncludedFolder(f, projectPath, excludedFiles))
						empty = false;
				}
			}
		}
		if (empty && !isExcluded(file, projectPath, excludedFiles)) {
			Files.delete(file.toPath());
			return true;
		}
		return false;
	}

	/**
	 * Returns the total number of files contained in a directory except vplignored
	 * ones (only including the files within specified sub-directories).
	 *
	 * @param folder The absolute path of the directory.
	 * @param subFolders The list of relative (to mainDir) paths of the
	 *                sub-directories.
	 *                To count all the files within mainDir, provide a list
	 *                containing only ".".
	 */
	public static int countIncludedFiles(String folder, List<String> subFolders, String projectPath,
										 List<String> excludedFiles) {
		int n = 0;
		for (String dir : subFolders)
			n += listIncludedFiles(folder + File.separator + dir, projectPath, excludedFiles).size();
		return n;
	}

	/**
	 * Returns a list of all the files contained within the specified directory.
	 *
	 * Note: This method lists every file without filtering.
	 * To list files with exceptions (e.g. IDE-specific files), use {@link #listIncludedFiles(String, String, List)} instead.
	 *
	 * @param rootDirectory The directory of which the files will be listed.
	 * @return A list containing all files in rootDirectory and in sub-directories.
	 *  This list does not include these directories, only the files they contain.
	 */
	public static List<File> listFiles(String rootDirectory) {
		List<File> files = new ArrayList<>();
		listFiles(rootDirectory, files);
		return files;
	}
	
	/**
	 * Recursive function helper for {@link #listFiles(String)}, incrementally building the list.
	 *
	 * @param folder The path where we are currently searching.
	 * @param collectedFiles The incrementally built list.
	 */
	private static void listFiles(String folder, List<File> collectedFiles) {
		File root = new File(folder);
		File[] files = root.listFiles();

		if (files == null) {
			return;
		}

		for (File file : files) {
			if (file.isDirectory()) {
				listFiles(file.getAbsolutePath(), collectedFiles);
			} else {
				collectedFiles.add(file);
			}
		}
	}
	
	/**
	 * Returns the total number of files contained in a directory (only including the files within specified sub-directories).
	 *
	 * Note: This method counts every file without filtering.
	 * To count files with exceptions (e.g. IDE-specific files), use {@link #countIncludedFiles(String, List, String, List)} instead.
	 *
	 * @param mainDir The absolute path of the directory.
	 * @param subDirs The list of relative (to mainDir) paths of the sub-directories.
	 *  To count all the files within mainDir, provide a list containing only ".".
	 */
	public static int countFiles(String mainDir, List<String> subDirs) {
		int n = 0;
		for(String dir : subDirs)
			n += listFiles(mainDir + File.separator + dir).size();
		return n;
	}

	
	/**
	 * Deletes the given File from disk (and contained files or sub-directories if applicable).
	 *
	 * Note: This method deletes every file without filtering.
	 * To delete files with exceptions (e.g. IDE-specific files), use {@link #deleteIncludedFolder(File, String, List)} instead.
	 *
	 * @param file The File to delete.
	 * @return {@code true} if adeletion was successful (i.e. files existed and were deleted), {@code false} otherwise.
	 * @throws IOException If an I/O error occurs.
	 *
	 */
	public static boolean deleteFromDisk(File file) throws IOException {
	    if (!file.exists()) {
	        return false;
	    }
        File[] contents = file.listFiles();
        if (contents != null){
        	// file is a directory - delete its contents first
            for (File f : contents) {
                if (!Files.isSymbolicLink(f.toPath()) && !deleteFromDisk(f)) {
            		return false;
                }
            }
        }
        Files.delete(file.toPath());
        return true;
	}


	/** Number of bytes to check for heuristics in {@link #isBinary(byte[])}. */
	private static final int FIRST_FEW_BYTES = 8000;
	
	/**
	 * Determines heuristically whether a byte array represents binary (as opposed to text) content.
	 * @param raw The raw content.
	 * @return {@code true} if raw is likely to be binary content, {@code false} otherwise.
	 */
	public static boolean isBinary(byte[] raw) {
		// Same heuristic as C Git
		for (int ptr = 0; ptr < Math.min(raw.length, FIRST_FEW_BYTES); ptr++)
			if (raw[ptr] == '\0')
				return true;
		
		return false;
	}

}
