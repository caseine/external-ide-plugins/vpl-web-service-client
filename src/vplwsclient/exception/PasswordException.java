package vplwsclient.exception;

import javax.json.JsonObject;

/**
 * Exception thrown when the VPL required a password, and no password is provided (or an incorrect one).
 * @author Astor Bizard
 */
public class PasswordException extends MoodleWebServiceException {
	
	private static final long serialVersionUID = -5257356654362131435L;

	public PasswordException(JsonObject jso) {
		super("requiredpassword", jso.getString("exception",""), jso.getString("message",""));
	}
}
