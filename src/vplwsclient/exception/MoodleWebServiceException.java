/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package vplwsclient.exception;

import javax.json.JsonObject;

/**
 * Exception thrown when a web-service answers with an error.
 * @author Astor Bizard
 */
public class MoodleWebServiceException extends VplException {

	private static final long serialVersionUID = 1254548021563997418L;
	
	private final String message;
	
	/**
	 * Constructor from a JsonObject.
	 * @param jso The response of the web-service containing the error.
	 */
	public MoodleWebServiceException(JsonObject jso) {
		this(jso.getString("errorcode",""), jso.getString("exception",""), jso.getString("message",""));
	}
	
	/**
	 * Constructor from error details.
	 * @param errorcode The error code.
	 * @param exception The exception name.
	 * @param message The exception message.
	 */
	public MoodleWebServiceException(String errorcode, String exception, String message){
		switch(errorcode){
			case "invalidtoken": this.message = "Invalid token, please provide a valid one.";
				break;
			case "nopermissions": this.message = "You do not have the permissions to access this VPL.\n" +
					"If you should have access to it, please check the VPL ID and your token, or call the platform administrator.";
				break;
			case "invalidcoursemodule": this.message = "Invalid VPL ID, please provide a valid one.";
				break;
			case "invalidparameter": this.message = "Unknown VPL ID or token, please provide them.";
				break;
			case "requiredpassword": this.message = "This VPL requires a password. You may have specified no password or a wrong one.";
				break;
			default: this.message = message + "\n[" + exception + " - " + errorcode + "]";
		}
	}
	
	@Override
	public String getMessage(){
		return message;
	}

	/**
	 * Creates a {@code MoodleWebServiceException} according to the given JsonObject.
	 * It is intended to be called only when the JsonObject contains an exception field.
	 * @param jso The JsonObject from which to build the exception.
	 * 		This should be the result of a call to a web-service.
	 * @return A newly created {@code MoodleWebServiceException}.
	 */
	public static MoodleWebServiceException create(JsonObject jso) {
		if(jso.getString("errorcode", "").equals("requiredpassword") || jso.getString("message","").equals("A password is required")) {
			// Password exceptions are often meant to be treated differently (retry with a password), so we create a specific exception.
			return new PasswordException(jso);
		}
		else {
			return new MoodleWebServiceException(jso);
		}
	}
}
