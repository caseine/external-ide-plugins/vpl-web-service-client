/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package vplwsclient.exception;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import javax.json.stream.JsonParsingException;

/**
 * Exception thrown when a problem occurs during communication with the web-service.
 * It acts as a wrapper for some IOExceptions and parsing exceptions.
 * @author Astor Bizard
 */
public class VplConnectionException extends VplException {

	private static final long serialVersionUID = 5242287405234794974L;
	
	private final String errorName;
	private final String errorDetails;
	
	/**
	 * Constructor from an Exception, typically an IOException or JsonParsingException
	 */
	public VplConnectionException(Exception e){
		super(e);
		if(e instanceof UnknownHostException)
			errorName = "Unknown host";
		else if(e instanceof FileNotFoundException)
			errorName = "Webservice not found at location";
		else if(e instanceof MalformedURLException)
			errorName = "Invalid webservice URL";
		else if(e instanceof JsonParsingException)
			errorName = "Unable to parse the response - URL may not be a valid webservice";
		else
			errorName = "VPL connection issue (" + e.getClass().getSimpleName() +")";
		
		errorDetails = e.getMessage();
	}
	
	@Override
	public String getMessage() {
		return errorName + ":\n" + errorDetails;
	}
}
