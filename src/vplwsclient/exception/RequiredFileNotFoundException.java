/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package vplwsclient.exception;

import java.util.List;

/**
 * Exception thrown when files required by the VPL are not found locally when trying to push files.
 * @author Astor Bizard
 */
public class RequiredFileNotFoundException extends VplException {
	
	private static final long serialVersionUID = -5778648586283120134L;

	private final List<String> files;
	
	/**
	 * Constructor from a list of not found files.
	 * @param files The required files that were not found locally
	 */
	public RequiredFileNotFoundException(List<String> files){
		this.files = files;
	}
	
	@Override
	public String getMessage() {
		StringBuilder strBuilder = new StringBuilder("Error: following required file(s) were not found:\n");
		for(String file : files) {
			strBuilder.append(file);
			strBuilder.append("\n");
		}	
		return strBuilder.toString();
	}
}
