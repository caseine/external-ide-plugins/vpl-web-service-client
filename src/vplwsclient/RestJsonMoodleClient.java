/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package vplwsclient;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParsingException;

import vplwsclient.exception.MaxFilesException;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.RequiredFileNotFoundException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

/**
 * Client for communication with a Moodle Rest VPL Webservice using Json format.
 * 
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 */
public class RestJsonMoodleClient {

	/**
	 * Pattern for token mask.
	 */
	private static final Pattern WSTOKEN_FILTER = Pattern.compile("wstoken=[^&]*+");
	
    private static final Logger LOGGER = Logger.getLogger( RestJsonMoodleClient.class.getPackage().getName() );

	public enum VPLService {
		VPL_INFO {
			@Override
			public String toString() {
				return "mod_vpl_info";
			}
		},
		VPL_SAVE {
			@Override
			public String toString() {
				return "mod_vpl_save";
			}
		},
		VPL_SAVE_EF {
			@Override
			public String toString() {
				return "mod_vpl_save_execution_files";
			}
		},
		VPL_SAVE_CF {
			@Override
			public String toString() {
				return "mod_vpl_save_corrected_files";
			}
		},
		VPL_SAVE_RF {
			@Override
			public String toString() {
				return "mod_vpl_save_required_files";
			}
		}
	}

	/** ID of the VPL this client will communicate with */
	private String vplID;
	/** User token */
	private String token;
	/** Web-service URL */
	private String url;
	/** Password to access VPL **/
	private String vplPassword;
	/**
	 * Wether the server we are connecting to supports the "isbinary" file field.
	 **/
	private boolean serverSupportsIsBinary;

	/**
	 * Constructor from VPL ID, user token and web-service URL.
	 */
	public RestJsonMoodleClient(String vplID, String token, String url) {
		this(vplID, token, url, "");
	}

	/**
	 * Constructor from VPL ID, user token, web-service URL and VPL password.
	 */
	public RestJsonMoodleClient(String vplID, String token, String url, String vplPassword) {
		this.vplID = vplID;
		this.token = token;
		this.url = url;
		this.vplPassword = vplPassword;
		this.serverSupportsIsBinary = false;
	}

	/**
	 * Sets the password for the VPL this client will be using.
	 */
	public void setPassword(String vplPassword) {
		this.vplPassword = vplPassword;
	}

	public void setServerSupportsIsBinary(boolean serverSupportsIsBinary) {
		this.serverSupportsIsBinary = serverSupportsIsBinary;
	}

	/**
	 * Makes a simple call to a web-service and returns the response. Do not use
	 * this method to call the "save" web-service - use the
	 * {@link #callServiceWithFiles} method instead.
	 * 
	 * @param serviceName   The web-service to call (e.g. "mod_vpl_info",
	 *                      "mod_vpl_evaluate", "mod_vpl_get_result", ...).
	 * @param serviceParams (optional) The parameters to use with the webservice.
	 *                      These must be specified as pairs &lt;name, value&gt;.
	 *                      Example:
	 *                      {@code callService("mod_vpl_set_config", "settingname", "maxfiles", "settingvalue", "1");}
	 * @return The response as a JsonObject.
	 * @throws VplConnectionException    If a connection error occurs.
	 * @throws MoodleWebServiceException If the web-service answers with an
	 *                                   exception.
	 */
	public JsonObject callService(String serviceName, String... serviceParams)
			throws VplConnectionException, MoodleWebServiceException {
		if (serviceParams.length % 2 != 0) {
			throw new IllegalArgumentException(
					"Service call parameters should be specified as String pairs: " + "[arg0, val0, arg1, val1, ...]");
		}

		String serverurl = buildWebserviceURL(serviceName);
		display(serverurl);
		StringBuilder strBuilder = new StringBuilder();

		for (int i = 0; i < serviceParams.length; i += 2) {
			strBuilder.append(URLEncoder.encode(serviceParams[i], StandardCharsets.UTF_8));
			strBuilder.append("=");
			strBuilder.append(URLEncoder.encode(serviceParams[i + 1], StandardCharsets.UTF_8));
			strBuilder.append("&");
		}

		String postParameters = strBuilder.toString();

		// Remove the trailing '&'
		if (postParameters.endsWith("&")) {
			postParameters = postParameters.substring(0, postParameters.length() - 1);
		}

		return connectAndGetResponse(serverurl, postParameters);
	}

	/**
	 * Extracts files from a web-service response object.
	 * 
	 * @param data  The response object obtained from a web-service.
	 * @param field The field containing the files in data.
	 * @return An array containing the files.
	 */
	public static VplFile[] extractFiles(JsonObject data, String field) {
		JsonArray jsonfiles = data.getJsonArray(field);
		VplFile[] resultfiles = new VplFile[jsonfiles.size()];
		for (int i = 0; i < jsonfiles.size(); i++)
			resultfiles[i] = new VplFile(jsonfiles.getJsonObject(i));
		return resultfiles;
	}

	private static final List<String> maxfilesConstrainedServices =
			Arrays.asList("mod_vpl_save", "mod_vpl_save_corrected_files", "mod_vpl_save_required_files");
	private static final List<String> reqfilesConstrainedServices =
			maxfilesConstrainedServices.subList(0,1);
	
	/**
	 * Push local files to a VPL.<br>
	 * 
	 * The matrix below describes constraints applied to the services used by current plugins.
	 * 
	 * <pre>
	 * |                                     | MaxFiles | ReqFiles | Student Plugin  | Teacher Plugin |
	 * |-------------------------------------|----------|----------|-----------------|----------------|
	 * | mod_vpl_save                        | x        | x        | x               |                |
	 * | mod_vpl_save_corrected_files        | x        |          |                 | x              |
	 * | mod_vpl_save_required_files         | x        |          |                 | x              |
	 * | mod_vpl_save_execution_files        |          |          |                 | x              |	
	 * </pre>
	 * @param serviceName The web-service to call (e.g. "mod_vpl_save", ...).
	 * @param listOfFiles An exhaustive list of files to send to the VPL.
	 * @throws IOException  If an I/O error occurs while reading local files.
	 * @throws VplException If an error occurs during the call to the web-service,
	 *                      or if it answers with an exception.
	 */
	public JsonObject callServiceWithFiles(String serviceName, List<VplFile> listOfFiles)
			throws IOException, VplException {
		String serverurl = buildWebserviceURL(serviceName);
		display(serverurl);

		boolean[] writtenFile = new boolean[listOfFiles.size()];
		Arrays.fill(writtenFile, false);

		StringBuilder strBuilder = new StringBuilder();
		int cpt = 0;

		JsonObject infoObject = callService("mod_vpl_info");
		JsonArray reqFiles = infoObject.getJsonArray("reqfiles");
		if (!reqFiles.isEmpty()) {
			this.serverSupportsIsBinary = reqFiles.getJsonObject(0).containsKey("isbinary");
			if (LOGGER.isLoggable(Level.INFO)) {
				String support = (this.serverSupportsIsBinary ? "does" : "does not");
				LOGGER.info(String.format("Detected that server %s support isbinary field.", support));
			}
		}
		int maxFiles = infoObject.getInt("maxfiles");

		if (listOfFiles.size() > maxFiles && maxfilesConstrainedServices.contains(serviceName)) {
			throw new MaxFilesException(maxFiles, listOfFiles.size());
		}
		
		if (reqfilesConstrainedServices.contains(serviceName)) {
			// First, we process the required files
			ArrayList<String> notFoundFiles = new ArrayList<>();
			for (int i = 0; i < reqFiles.size(); ++i) {
				String rfName = reqFiles.getJsonObject(i).getString("name");
				boolean b64required = rfName.endsWith(".b64"); // Legacy: the required file on the server is
																// base64-encoded.
				if (LOGGER.isLoggable(Level.INFO)) {
					LOGGER.info(String.format("Searching for required file %s ... ", rfName));
				}
				boolean found = false;
				for (int j = 0; j < listOfFiles.size(); j++) {
					// Search this required file among the local files
					VplFile f = listOfFiles.get(j);
					String fName = f.getFullName();
					if (b64required && f.isBinary()) {
						fName += ".b64";
					}
					if (fName.equals(rfName)) {
						// We found the required file
						if (LOGGER.isLoggable(Level.INFO)) {
							LOGGER.info(String.format("Found at %s", f.getFullOSName()));
						}
						strBuilder.append(this.buildURLParameterFromFile(f, cpt++, b64required));
						strBuilder.append("&");
						writtenFile[j] = found = true;
						break;
					}
				}
				if (!found) {
					// We didn't find the required file
					LOGGER.severe("Not found!");
					notFoundFiles.add(rfName);
				}
			}

			if (!notFoundFiles.isEmpty()) {
				// Some required files were not found : stop and display an error
				throw new RequiredFileNotFoundException(notFoundFiles);
			}
		}

		// Process all the other files
		for (int j = 0; j < listOfFiles.size(); j++) {
			VplFile f = listOfFiles.get(j);
			if (!writtenFile[j]) {
				// We found an additional file
				if (LOGGER.isLoggable(Level.INFO)) {
					LOGGER.info(String.format("Found additional file at %s", f.getFullOSName()));
				}
				strBuilder.append(this.buildURLParameterFromFile(f, cpt++));
				strBuilder.append("&");
			}
		}

		String postParameters = strBuilder.toString();

		// Remove the trailing '&'
		if (postParameters.endsWith("&")) {
			postParameters = postParameters.substring(0, postParameters.length() - 1);
		}

		return connectAndGetResponse(serverurl, postParameters);
	}

	/**
	 * Builds the web-service URL to be used with this client.
	 * 
	 * @param serviceName The web-service to call (e.g. "mod_vpl_info",
	 *                    "mod_vpl_evaluate", "mod_vpl_get_result", ...).
	 * @return The built URL.
	 */
	private String buildWebserviceURL(String serviceName) {
		return url + "?wstoken=" + token + "&wsfunction=" + serviceName + "&id=" + vplID
				+ "&moodlewsrestformat=json&password=" + vplPassword;
	}

	/**
	 * Builds an URL-encoded string representing the specified file data to be sent
	 * as a service parameter.
	 * 
	 * @param file  The file to write.
	 * @param index The index of the file in the final files array.
	 * @return The built URL-encoded string.
	 * @throws IOException If an I/O error occurs.
	 */
	private String buildURLParameterFromFile(VplFile file, int index) throws IOException {
		return this.buildURLParameterFromFile(file, index, false);
	}

	/**
	 * Builds an URL-encoded string representing the specified file data to be sent
	 * as a service parameter.
	 * 
	 * @param file        The file to write.
	 * @param index       The index of the file in the final files array.
	 * @param keepEncoded If true, the file will be declared not binary in every
	 *                    case, so the server will not decode it.
	 * @return The built URL-encoded string.
	 * @throws IOException If an I/O error occurs.
	 */
	private String buildURLParameterFromFile(VplFile file, int index, boolean keepEncoded) throws IOException {
		String name = file.getFullName();
		if (file.isBinary() && keepEncoded) {
			name += ".b64";
		}
		StringBuilder strBuilder = new StringBuilder("files[" + index + "][name]="
				+ URLEncoder.encode(name, StandardCharsets.UTF_8) + "&files[" + index + "][data]=");
		BufferedReader br = new BufferedReader(new StringReader(file.getContents()));
		String line;
		while ((line = br.readLine()) != null) {
			strBuilder.append(URLEncoder.encode(line + "\n", StandardCharsets.UTF_8));
		}
		br.close();
		if (this.serverSupportsIsBinary) {
			strBuilder.append("&files[" + index + "][isbinary]=" + (file.isBinary() && !keepEncoded ? "1" : "0"));
		}
		return strBuilder.toString();
	}

	private static JsonObject connectAndGetResponse(String fullUrl, String postParameters)
			throws VplConnectionException, MoodleWebServiceException {

		// Send request
		JsonObject response;
		try {
			HttpURLConnection con = (HttpURLConnection) new URL(fullUrl).openConnection();
			if (postParameters.length() > 0) {
				con.setRequestMethod("POST");
				con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				con.setRequestProperty("Content-Language", "en-US");
				con.setDoOutput(true);
				con.setUseCaches(false);
				DataOutputStream wr = new DataOutputStream(con.getOutputStream());
				wr.writeBytes(postParameters);
				wr.close();
			}
			response = getResponse(con.getInputStream());
		} catch (IOException | JsonParsingException e) {
			throw new VplConnectionException(e);
		}
		if (response != null && response.containsKey("exception")) {
			throw MoodleWebServiceException.create(response);
		}
		return response;
	}

	/**
	 * Reads the web-service response on the specified stream and returns it as a
	 * JsonObject.
	 * 
	 * @param is The stream on which the response will be read.
	 * @return The response as a JsonObject.
	 * @throws IOException          If an I/O error occurs.
	 * @throws JsonParsingException If a parsing error occurs.
	 */
	private static JsonObject getResponse(InputStream is) throws IOException, JsonParsingException {
		JsonObject result = null;
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		String line;
		StringBuilder response = new StringBuilder();
		while ((line = rd.readLine()) != null) {
			response.append(line);
			response.append('\r');
		}
		rd.close();
		if (!response.toString().startsWith("null")) {
			try (JsonReader reader = Json.createReader(new StringReader(response.toString()))) {
				result = reader.readObject();
			}
		}
		return result;
	}

	/**
	 * Simply displays the serverurl for debug purpose.
	 * 
	 * @param serverurl the server url
	 */
	private static void display(String serverurl) {
		if (LOGGER.isLoggable(Level.INFO) ) {
			Matcher m = WSTOKEN_FILTER.matcher(serverurl);
			if (m.find()) {
				String originalToken = m.group(0);
				if (originalToken.length() >= 18) {
					String token = "wstoken=" + "********" + originalToken.substring(originalToken.length() - 10);
					LOGGER.info(serverurl.replace(originalToken, token));
				}
			}
		}
	}
}
