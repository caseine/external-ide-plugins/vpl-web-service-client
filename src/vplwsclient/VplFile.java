/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package vplwsclient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Base64;

import javax.json.JsonObject;

/**
 * Convenient representation of a file to be sent / received from a VPL, managing Base64 encoding/decoding for binary files.
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 */
public class VplFile {
	
	private String name;
	private String contents;
	private boolean isBinary;

	/**
	 * Constructor from a JsonObject (most likely a part of a response from a web-service).
	 * @param file The JsonObject from which to build the VplFile. It must contain the fields "name" and "data".
	 */
	public VplFile(JsonObject file) {
        name = file.getString("name");
        contents = file.getString("data");

        isBinary = file.getBoolean("isbinary", false) || name.endsWith(".b64"); // Legacy: the required file on the server is base64-encoded.
        if (isBinary) {
        	if (name.endsWith(".b64")) {
        		name = name.substring(0, name.length() - 4);
        	}
    		contents = contents.replaceAll("[\\r\\n]+", "");
        }
	}
	
	/**
	 * Constructor from a (local) File.
	 * @param file The File from which to build the VplFile.
	 * @param vplPath The full name of this file as expected by a web-service.
	 * @throws IOException If an I/O error occurs.
	 */
	public VplFile(File file, String vplPath) throws IOException {
        name = vplPath;
        byte[] fileContents = Files.readAllBytes(file.toPath());
        isBinary = FileUtils.isBinary(fileContents);
        if (isBinary) {
        	contents = Base64.getEncoder().encodeToString(fileContents);
        } else {
        	contents = new String(fileContents, StandardCharsets.UTF_8);
        }
	}
	
	/**
	 * Returns the full name of this file on the VPL (including its path).
	 * The eventual trailing ".b64" has been removed.
	 */
	public String getFullName() {
		return name;
	}

	/**
	 * Returns the full name of this file on the VPL (including its path), encoded with OS file separator.
	 * The eventual trailing ".b64" has been removed.
	 * This method is equivalent to {@link #getFullName()}{@code .replace("/", File.separator)}.
	 */
	public String getFullOSName() {
		return name.replace("/", File.separator);
	}
	
	/**
	 * Returns the path of this file on the VPL (not including the file name), without the trailing separator.
	 */
	public String getPath() {
		int pathLength = name.lastIndexOf("/");
		return (pathLength > 0) ? name.substring(0, pathLength) : "";
	}

	/**
	 * Returns the path of this file on the VPL (not including the file name), without the trailing separator, encoded with OS file separator.
	 * This method is equivalent to {@link #getPath()}{@code .replace("/", File.separator)}.
	 */
	public String getOSPath() {
		return getPath().replace("/", File.separator);
	}

	/**
	 * Returns the text contents of the file (if the file is base64-encoded, this returns the base64-encoded text).
	 */
	public String getContents() {
		return this.contents;
	}

	/**
	 * Returns the bytes of content of the file. The contents are decoded if the file is a base64-encoded binary.
	 */
	public byte[] getBytes() {
		if (this.isBinary()) {
			return Base64.getDecoder().decode(this.contents.getBytes(StandardCharsets.UTF_8));
		} else {
			return this.contents.getBytes();
		}
	}
	
	/**
	 * Returns whether this file represents a base64-encoded file or not.
	 * * @deprecated Use {@link #isBinary()} instead.
	 */
	@Deprecated(since="1.0.0", forRemoval=true)
	public boolean isBase64() {
		return this.isBinary;
	}
	
	/**
	 * Returns whether this file represents a binary file.
	 */
	public boolean isBinary() {
		return this.isBinary;
	}

	/**
	 * Writes this file to the specified FileOutputStream. If this file is base64-encoded, it will be decoded before writing.
	 * This is equivalent to {@code fos.write(file.getBytes())}.
	 * @param fos The stream to which the file should be written.
	 * @throws IOException If an I/O error occurs.
	 */
	public void write(FileOutputStream fos) throws IOException {
		fos.write(this.getBytes());
	}

}
