# Deployment to maven

- Be in a CLEAN state (everything up-to-date with master, no untracked files).
- Run:  
`mvn clean install -Dskip=false`  
`mvn clean release:prepare  -Dskip=false --batch-mode`  
`git log` (check that commit and tag have been created)  
`mvn clean release:perform -Dskip=false`  
`git push --follow-tags`
- If something goes wrong, go back to step 1 with cancelled commits.
- If a perform was tried, it is necessary to either retry with a version increment or delete submission on [sonatype](https://oss.sonatype.org/#stagingRepositories).
- The push to sonatype/mvnrepository is done via release:perform, using credentials in .m2/settings.xml.