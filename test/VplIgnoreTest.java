
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import vplwsclient.FileUtils;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class VplIgnoreTest {

    private boolean testListIncludedFiles(String projectPath, String ... expectedFiles) {
        return completeTestListIncludedFiles(projectPath, projectPath, projectPath, expectedFiles);
    }
    private boolean completeTestListIncludedFiles(String projectPath, String vplignoreFolder, String searchedFolder, String ... expectedFiles) {
        List<File> expected = new ArrayList<>();
        for (String expectedFile : expectedFiles) {
            expected.add(Paths.get(expectedFile).toAbsolutePath().normalize().toFile());
        }
        List<String> excludedFiles = FileUtils.scanExcludedList(vplignoreFolder);
        String absoluteProjectPath = new File(projectPath).getAbsolutePath();
        List<File> actual = FileUtils.listIncludedFiles(searchedFolder, absoluteProjectPath, excludedFiles);
        boolean res = (expected.size() == actual.size()) && (expected.containsAll(actual));
        if (!res)
            System.out.println("Test of listIncludedFiles failed\nexpected : "+expected+"\nactual : "+actual+"\n");

        List<String> subdirs = new ArrayList<>();
        subdirs.add(".");
        int countedFiles = FileUtils.countIncludedFiles(searchedFolder, subdirs, absoluteProjectPath, excludedFiles);
        if (countedFiles != expected.size()) {
            System.out.println("Test of countIncludedFiles failed\nexpected : " + expected.size() + "\nactual : " + countedFiles);
            res = false;
        }
        return res;
    }

    @Test
    void SameLevelTest() {
        Assertions.assertTrue(testListIncludedFiles(
                "test/resources/SameLevelTest/",
                "test/resources/SameLevelTest/b.txt",
                "test/resources/SameLevelTest/.vplignore"
        ));
    }

    @Test
    void noIgnoreTest() {
        Assertions.assertTrue(testListIncludedFiles(
                "test/resources/noIgnoreTest/",
                "test/resources/noIgnoreTest/.settings/a.txt",
                "test/resources/noIgnoreTest/c.pyc",
                "test/resources/noIgnoreTest/d.pyo",
                "test/resources/noIgnoreTest/.project",
                "test/resources/noIgnoreTest/b.txt",
                "test/resources/noIgnoreTest/e$py.class",
                "test/resources/noIgnoreTest/.classpath",
                "test/resources/noIgnoreTest/a.txt",
                "test/resources/noIgnoreTest/.pydevproject"
        ));
    }

    @Test
    void wildCardExcludeTest() {
        Assertions.assertTrue(testListIncludedFiles(
                "test/resources/wildCardExcludeTest/",
                "test/resources/wildCardExcludeTest/b.txt",
                "test/resources/wildCardExcludeTest/.vplignore"
        ));
    }

    @Test
    void folderExcludeTest() {
        Assertions.assertTrue(testListIncludedFiles(
                "test/resources/folderExcludeTest/",
                "test/resources/folderExcludeTest/.vplignore",
                "test/resources/folderExcludeTest/a.txt"
        ));
    }

    @Test
    void differentRootTest() {
        Assertions.assertTrue(completeTestListIncludedFiles( // root is changedRootTest
                "test/resources/changedRootTest/",
                "test/resources/changedRootTest/vplignoreFolder/",
                "test/resources/changedRootTest/",
                "test/resources/changedRootTest/vplignoreFolder/b.txt",
                "test/resources/changedRootTest/vplignoreFolder/.vplignore",
                "test/resources/changedRootTest/vplignoreFolder/a.txt",
                "test/resources/changedRootTest/b.txt",
                "test/resources/changedRootTest/newRoot/b.txt",
                "test/resources/changedRootTest/newRoot/a.txt"
        ));
        Assertions.assertTrue(completeTestListIncludedFiles( // root is newRoot, look at changedRootTest
                "test/resources/changedRootTest/newRoot/",
                "test/resources/changedRootTest/vplignoreFolder/",
                "test/resources/changedRootTest/",
                "test/resources/changedRootTest/vplignoreFolder/b.txt",
                "test/resources/changedRootTest/vplignoreFolder/.vplignore",
                "test/resources/changedRootTest/vplignoreFolder/a.txt",
                "test/resources/changedRootTest/b.txt",
                "test/resources/changedRootTest/a.txt",
                "test/resources/changedRootTest/newRoot/b.txt"
        ));
        Assertions.assertTrue(completeTestListIncludedFiles( // root is newRoot, look at newRoot
                "test/resources/changedRootTest/newRoot/",
                "test/resources/changedRootTest/vplignoreFolder/",
                "test/resources/changedRootTest/newRoot/",
                "test/resources/changedRootTest/newRoot/b.txt"
        ));
    }
}
